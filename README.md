# Projet Titre Pro

## Remerciments

Depuis mon entrée dans l'entreprise SWORD, mes collègues ont toujours été disponibles pour me former au métier, c'est donc tout naturellement que je tenais a leur exprimer mes plus sincères remerciements.
J'ai particulièrement apprécié leur accueil lors de mon arrivé, cette première prise de contact a facilitée bien des choses par rapport à mon autisme.
De plus leurs explications m'ont toujours parues très claires, et il m'a été possible très rapidement de comprendre le fonctionnement de l'entreprise.
J'ai bien conscience que ma période de contrat pro s'en est trouvée plus efficace et plus probante et que, si la direction me permet, a l'avenir, de poursuivre sur un CDD ou CDI ils y seront pour beaucoup.
C'est pourquoi je voudrais encore une fois exprimer toute ma reconnaissance pour tout le temps qu'ils ont bien voulu me consacrer.

## Présentation du projet

Le but de ce projet est d'utiliser les differentes competences aquises durant les cours, et d'en developper de nouvelles grace a l'auto-formation. Pour savoir sur quel type de projet je devais travailler j'ai conseil a SWORD, qui m'as fait savoir qu'ils avaient eventuellement besoin d'un site "maquette" en symfony, et j'ai donc commencer a travailler sur une reproduction du site "AirBNB" en Symfony.

## Languages utilisées

* **HTML 5**
* **CSS 3**
* **MYSQL**
* **PHP 7**
* **TWIG**
* **SASS**
* **SCSS**

## Interet

1. Mise en pratique des connaissances acquise durant la formation Simplon

2.Acquisition de nouvelles connaissances

3. utilisation et apprentissage de **SYMFONY 4**, **DOCTRINE**, **PHP 7**, **MYSQL**, **WEBPACK** et **SASS**
 
## Modifications

*J'ai tout d'abord tenue a avoir une premiere version du site entierement fonctionnelle (ver:1.0)

**les modifications a venir concerneront une ver:2.0:**

* pouvoir changer les thèmes du back et du front via un formulaire.

* Ajouter une barre de "trie" afin de pouvoir chercher les annonces dans un certaine ordre(Date, alphabetique). 



## Exemple de code utilisé

Sélection d'une fonction de mon code (ArticleController) **PHP 7**

```
/**
     * permet de creer une annonce
     * 
     * @Route("/ads/new", name="ads_create")
     * @IsGranted("ROLE_USER")
     *
     * @return response
     */
    public function create(Request $request, ObjectManager $manager){
        $ad = new Ad();

        $form = $this->createForm(AdType::class, $ad);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            foreach($ad->getImages() as $image) {
                $image->setAd($ad);
                $manager->persist($image);
            }

            $ad->setAuthor($this->getUser());

            $manager->persist($ad);
            $manager->flush();

            $this->addFlash(
                'success',
                "L'annonce <strong>{$ad->getTitle()}</strong> a bien été enregistrée !"
            );

            return $this->redirectToRoute('ads_show', [
                'slug' => $ad->getSlug()
            ]);
        }

        return $this->render('ad/new.html.twig', [
            'form' => $form->createView()
        ]);
    }

```
Et ici un exemple d'une de mes pages Template
```
      <div class="ad-title" style="background-image: url({{ ad.coverImage }})">
    <div class="container">
        <h1>{{ad.title}}</h1>
        <p>{{ad.introduction | raw}}</p>
        <p class="h2">
            <strong>{{ad.rooms}}
            chambres</strong>
             pour<strong>{{ad.price | number_format(2, ',', ' ')}}
                €</strong>
             / nuit</p>
        <a href="{{ path('booking_create', {'slug': ad.slug})}}" class="btn btn-primary">Réservé</a>
        {% if app.user and app.user == ad.author %}
            <a href="{{ path('ads_edit', {'slug': ad.slug}) }}" class="btn btn-secondary">Modifier l'annonce</a>
            <a href="{{ path('ads_delete', {'slug': ad.slug}) }}" class="btn btn-danger" 
            onclick="return confirm(`Etes-vous sur de vouloir supprimer l'annonce : {{ ad.title }} ?`)">Supprimer l'annonce</a>
        {% endif %}
    </div>
</div>


<div class="container">
    <div class="row">
        <div class="col-md-8 mb-3">
            {{ad.content | raw}}
            <hr>

            {# Debut du carrousel #}
             <div id="carouselExampleCaptions" class="slick slider">
                {% for image in ad.images %}
                <div class="slick-item">
                    <img src="{{image.url}}" class="d-block w-100" alt="">
                    <div class="item-caption d-none d-md-block">
                        <h5>{{image.caption}}</h5>
                    </div>
                </div>
                {% endfor %}
            </div>
            <hr>
    
```
## Diagramme Use Case

![20% center](./docREADME/use_case.png)

## Base de données SQL

![20% center](./docREADME/sql.png)



## Site

voici la page d'acceuil de mon blog a partir de laquel on peux se deplacer sur la liste des articles via 2 boutons (celui de la nav-bar et le bouton "articles -->"):

![20% center](./doc/acceuil-blog.png)

la liste des articles que j'ai agrementé avec un petit badge, et un bouton "read" pour voir l'article en détaille:

![20% center](./doc/article-blog.png)

le formulaire pour ajouté ou modifié un article:

![20% center](./doc/form-blog.png)

et la vue détaillée d'un article que l'on peux supprimé ou modifié grace au boutons associé:

![20% center](./doc/see-article-blog.png)

## Difficultés

La création de la base de données n'as pas été trés difficile, le plus dur a été de comprendre le fonctionnement de **workbench**, en revanche j'ai eu de grosses difficultés a comprendre le fonctionnement de **PDO** dans **SYMFONY 4**. 

## Axes d'amélioration

Cela m'a poussé à basculer sur **DOCTRINE** qui m'as pas mal simplifié la vie et permis de faire un **CRUD** assez simplement.

## Ce que j'ai aimé faire

Ma partie préféré a été le front que j'ai réalisé avec **BOOTSWATCH**(pour le thème et la NAV-BAR) et **BOOTSTRAP**(pour l'acceuil et les articles).

## Pour lancer mon projet

1. cloner le projet
2. Installer les dépendences avec composer install
3. Mettre vos infos dans le .env (DATABASE_URL=mysql://name:Password@127.0.0.1:3306/BDDname)
4. Faire un php bin/console doctrine:database:create
5. Faire un php bin/console doctrine:migrations:migrate
6. Faire un php bin/console doctrine:fixtures:load
7. Faire un npm i
8. Faire un tarn install
9. Faire un yarn encore dev
10. Faire un php bin/console c:c
11. Faire une php bin/console server:run pour demarrer le serveur

## Remerciments

Depuis mon entrée dans l'entreprise SWORD, mes collègues ont toujours été disponibles pour me former au métier, c'est donc tout naturellement que je tenais a leur exprimer mes plus sincères remerciements.
J'ai particulièrement apprécié leur accueil lors de mon arrivé, cette première prise de contact a facilitée bien des choses par rapport à mon autisme.
De plus leurs explications m'ont toujours parues très claires, et il m'a été possible très rapidement de comprendre le fonctionnement de l'entreprise.
J'ai bien conscience que ma période de contrat pro s'en est trouvée plus efficace et plus probante et que, si la direction me permet, a l'avenir, de poursuivre sur un CDD ou CDI ils y seront pour beaucoup.
C'est pourquoi je voudrais encore une fois exprimer toute ma reconnaissance pour tout le temps qu'ils ont bien voulu me consacrer.

## Copyright

©2019-2020 DavidSalvador All right reserved.

(pour voir mes autre création rendez-vous sur [Gitlab](https://gitlab.com/dashboard/projects).)
