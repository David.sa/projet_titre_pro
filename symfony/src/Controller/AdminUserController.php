<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\AccountType;
use App\Form\AdminAccountType;
use App\Repository\UserRepository;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class AdminUserController extends AbstractController
{
    /**
     * @Route("/admin/users", name="admin_user_index")
     */
    public function index(UserRepository $repo)
    {
        return $this->render('admin/user/index.html.twig', [
            'users' => $repo->findAll()
        ]);
    }

    /**
     * Permet d'éditer un profil utilisateur
     * 
     * @Route("/admin/users/{id}/edit", name="admin_user_edit")
     * 
     * @return Response
     */
    public function edit(User $user, Request $request, ObjectManager $manager) {        
        $form = $this->createForm(AdminAccountType::class, $user);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {

            $manager->persist($user);

            $manager->flush();

            $this->addFlash(
                'success', 
                "Le profil de {$user->getFullName()} a bien été modifiée" 
            );

            return $this->redirectToRoute("admin_user_index");
        }

        return $this->render('admin/user/edit.html.twig', [
            'form' => $form->createView(),
            'user' => $user
        ]);
    }


     /**
     * Permet de supprimer un profil
     * 
     * @Route("/admin/users/{id}/delete", name="admin_user_delete")
     *
     * @return Response
     */
    public function delete(User $user, ObjectManager $manager) {
      
        if(count($user->getBookings()) > 0) {
            $this->addFlash(
                'warning',
                "Vous ne pouvez pas supprimer l'annonce <strong>{$user->getFullName()}</strong> car elle possède déjà des réservations ! <br> 
                Veuillez supprimer toute ces reservation pour supprimer le profil !"
            );
        } else {
            $manager->remove($user);
            $manager->flush();
    
            $this->addFlash(
                'success',
                "L'annonce <strong>{$user->getFullName()}</strong> a bien été supprimée !"
            );
        }


        return $this->redirectToRoute("admin_user_index");
    }

}
