<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200103221350 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE ad ADD address_id INT DEFAULT NULL, DROP address');
        $this->addSql('ALTER TABLE ad ADD CONSTRAINT FK_77E0ED58F5B7AF75 FOREIGN KEY (address_id) REFERENCES announce_address (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_77E0ED58F5B7AF75 ON ad (address_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE ad DROP FOREIGN KEY FK_77E0ED58F5B7AF75');
        $this->addSql('DROP INDEX UNIQ_77E0ED58F5B7AF75 ON ad');
        $this->addSql('ALTER TABLE ad ADD address VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, DROP address_id');
    }
}
