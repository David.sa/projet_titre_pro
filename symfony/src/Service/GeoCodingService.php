<?php

namespace App\Service;

use mysql_xdevapi\Exception;
use phpDocumentor\Reflection\Types\String_;
use Symfony\Component\HttpClient\HttpClient;

class GeoCodingService
{
    //Je defini les constantes
    const GEOCODE_API_URL = 'http://nominatim.openstreetmap.org/search?';
    const GEOCODE_API_FORMAT = 'json';
    const GEOCODE_API_LIMIT = 1;
    const AUTHORIZED_PARAMS = [
        'street' => 'string',
        'city' => 'string',
        'postalcode' => 'integer',
        'county' => 'string',
        'country' => 'string',
        'state' => 'string'
    ];

    /**
     * https://wiki.openstreetmap.org/wiki/FR:Nominatim#Param%C3%A8tres
     */
    const AUTHORIZED_FORMAT = [
        'xml' => 'decodeXml',
        'json' => 'decodeJson',
        'jsonv2' => 'decodeJson',
        'html' => 'decodeHtml'
    ];

    public function __construct()
    {
        //Je verifie que la valeur de GEOCODE_API_FORMAT est bien presente dans le tableau AUTHORIZED_FORMAT
        $authorizedFormat = array_keys(self::AUTHORIZED_FORMAT);
        if(!in_array(self::GEOCODE_API_FORMAT, $authorizedFormat))
            throw new \Exception('Format not authorized, please verify GEOCODE_API_FORMAT');
    }

    /**
     * getLongLat
     * https://wiki.openstreetmap.org/wiki/FR:Nominatim
     * @param array $addressInformations array of address's params (street[string], city[string], county[string], country[string], postcode[int], state[string])
     */
    public function getLongLat($addressInformations)
    {
        // J'exclu les parametres non désiré et je set ma request
        $parametersSanitized = $this->checkParameters($addressInformations);
        $request = $this->buildRequest($parametersSanitized);
        // je concatene le format a ma request
        $request = $this->buildRequestFormat($request);
        $request = $this->buildLimitResult($request);
        // Concat url + request
        $url = self::GEOCODE_API_URL . $request;
        $response = $this->getResponse($url);
        // je recupere les reponses voulu
        $longLat = [
            'long' => $response[0]->lon,
            'lat' => $response[0]->lat,
        ];

        return $longLat;
    }

    /**
     * @param array $addressInformations Informations about the request
     */
    protected function checkParameters($addressInformations)
    {
        foreach(self::AUTHORIZED_PARAMS as $k => $v){
            // Je verifie que les entrée de $addressInformation ne sont ni null ni vide et du bon type
            $x = null;
            if(isset($addressInformations[$k]) && !is_null($addressInformations[$k])){
                switch ($v){
                    case 'string':
                        $x = $this->checkString($addressInformations[$k]);
                        break;
                    case 'integer':
                        $x = $this->checkInteger($addressInformations[$k]);
                        break;
                    default;
                        break;
                }
            };

            if (isset($x)){
                $array[$k]=$x;
            }
        }
        return $array;
    }

    protected function buildRequest($addressInformations)
    {
        // exemple: street=4+rue+des+fleurs&city=Lyon&=postalcode=69004
        // je verifie que $addressInformation est bien un array sinon je leve une exeption
        if(!is_array($addressInformations))
            throw new \Exception('$addressInformations should be an array');

        // je construit ma requete avec les informations de addressinformation + urlencode
        $request = '';
        foreach($addressInformations as $k => $information) {
            if (!empty($request))
                $request .= "&";

            $encodedInformation = urlencode($information);
            $request .= "$k=$encodedInformation";
        }
        return $request;

    }

    protected function buildRequestFormat($request)
    {
        //Je verifie que ma requete est du bon type
        if(!is_string($request))
            throw new \Exception('$request should be a string');
        //je complete ma requete avec le format desiré
        $request .= '&format='. self::GEOCODE_API_FORMAT;
        return $request;
    }

    protected function buildLimitResult($request)
    {
        //Je verifie que ma requete est du bon type
        if(!is_string($request))
            throw new \Exception('$request should be a string');
        //je concatene un parametre suppl a ma requete pour recevoir un nombre limité de reponse
        $request .= '&limit='. self::GEOCODE_API_LIMIT;
        return $request;
    }

    protected function getResponse($url)
    {
        // create curl resource
        $client = HttpClient::create();
        $response = $client->request('GET', $url);

        $apiResponse = $response->getContent();

        //if(empty($apiResponse))
        //throw new \Exception('API NOMINATIM response empty');

        $decodeMethod = self::AUTHORIZED_FORMAT[self::GEOCODE_API_FORMAT];
        $responseDecoded = $this->$decodeMethod($apiResponse);

        return $responseDecoded;
    }

    protected function decodeXml($response)
    {
        $response = simplexml_load_string(utf8_encode($response));
        return $response;
    }

    protected function decodeJson($response)
    {
        //je decode la reponse
        $response = json_decode($response);
        return $response;
    }

    protected function decodeHtml($response)
    {
        //Si ma reponse est au format HTML je leve une exeption
        throw new \Exception('HTML format is not taken, add an handler for it or use another format');
    }

    protected function checkString($var)
    {
        if(!is_string($var)){
            throw new Exeption('Type de données incorrecte');
        }
        return $var;
    }

    protected function checkInteger($var)
    {
        $var = intval($var);
        return $var;
    }
}