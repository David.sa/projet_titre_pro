<?php

namespace App\Repository;

use App\Entity\AnnounceAddress;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method AnnounceAddress|null find($id, $lockMode = null, $lockVersion = null)
 * @method AnnounceAddress|null findOneBy(array $criteria, array $orderBy = null)
 * @method AnnounceAddress[]    findAll()
 * @method AnnounceAddress[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AnnounceAddressRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, AnnounceAddress::class);
    }

    // /**
    //  * @return AnnounceAddress[] Returns an array of AnnounceAddress objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?AnnounceAddress
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
