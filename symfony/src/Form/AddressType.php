<?php

namespace App\Form;

use App\Entity\AnnounceAddress;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AddressType extends ApplicationType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('address',
                TextType::class,
                $this->getConfiguration("Adresse", "Tapez l'adresse de votre bien")
            )

            ->add('city',
                TextType::class,
                $this->getConfiguration("Ville", "Tapez la ville de votre bien")
            )

            ->add('postCode',
                IntegerType::class,
                $this->getConfiguration("Code postale", "Tapez le code postale de votre bien")
            )
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => AnnounceAddress::class,
        ]);
    }
}
