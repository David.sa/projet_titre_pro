
$('.animate-magic').each(function(){
    $(this).on('click',function(){
        // Animate button if all field are filled
        submitAnimation($(this));
    });
});

function submitAnimation(elem)
{
    let startDate = $('#booking_startDate').val();
    let endDate = $('#booking_endDate').val();
    // If startDate and endDate 
    if(startDate.length != 0 && endDate.length != 0){
        // Animate button
        console.log('submit anim',elem);
        var anim = elem.data('animation');
        elem.addClass(anim);
    }else{
        alert('Tous les champs requis ne sont pas remplis');
    }
}
