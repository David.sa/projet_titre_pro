$( document ).ready(function() {

    $('#add-image').click(function(){
        // Je récupère le numéro des futurs champs que je vais créer
        let index = +$('#widgets-counter').val();

        // Je récupère le prototype des entrées
        let tmpl = $('#ad_images').data('prototype').replace(/_name_/g, index);

        // J'injecte ce code au sein de la div
        $('#ad_images').append(tmpl);

        $('#widgets-counter').val(index + 1);

        // Je gère le boutton "supprimer"
        handleDeleteButtons()
    });


    function handleDeleteButtons(){
         $('button[data-action="delete"]').click(function(){
            let target = this.dataset.target;
            $(target).remove();
        });
    }

    function updateCounter(){
        let count = +$('#ad_images div.form-group').length;

        $('#widgets-counter').val(count);
    }

    updateCounter();
    handleDeleteButtons();
});